"use strict";

(function main() {
  var COOKIE_NAME = 'PCXT';
  var COOKIE_REGEX = new RegExp("(^| )".concat(COOKIE_NAME, "=([^;]+)")); // Create the base pricingContext object, to be exported globally

  var contextModule = {}; // Set the version of this object. Used when introducing new changes to the context module.

  contextModule.version = '0.1.0'; // Keep an in-memory version of the cookie just in case `document.cookie` is inaccessible (e.g. during static page generation)

  var inMemoryCookie = '';
  /* --- Internal Helper Methods --- */

  /*
    Returns a url-friendly context string that represents the context object.
  */

  var encodeContextObject = function encodeContextObject(contextObj) {
    var base64 = btoa(JSON.stringify(contextObj)); // Replace chars in the base64 string to make it url safe per the RFC spec
    // https://tools.ietf.org/html/rfc4648#page-7

    return base64.replace(/\+/g, '-').replace(/=/g, '').replace(/\//g, '_');
  };
  /*
    Returns the context object that the context string represents.
  */


  var unencodeContextString = function unencodeContextString(contextString) {
    var replacedChars = contextString.replace(/-/g, '+').replace(/_/g, '/');
    return JSON.parse(atob(replacedChars));
  };

  var setCookieContext = function setCookieContext(context) {
    // Set expiration to a week
    var now = new Date();
    var expireDate = new Date(now.getTime() + 1000 * 604800);
    var encodedContext = encodeContextObject(context);
    var cookieString = "".concat(COOKIE_NAME, "=").concat(encodedContext, ";expires=").concat(expireDate.toUTCString()).concat(";path=/");

    if (typeof document !== 'undefined' && typeof document.cookie !== 'undefined') {
      document.cookie = cookieString;
    } else {
      inMemoryCookie = cookieString;
    }
  };

  var getDefaultContext = function getDefaultContext() {
    // TODO: make this function more dynamic. Look at current site to determine defaults.
    return {
      market: 'IE',
      couponCode: '',
      culture: 'en-IE',
      effectiveDateTime: '',
      testIds: [],
      vatInclusive: true
    };
  };

  var getRawCookieValue = function getRawCookieValue() {
    var cookieString = typeof document !== 'undefined' && typeof document.cookie !== 'undefined' ? document.cookie : inMemoryCookie;

    try {
      var match = cookieString.match(COOKIE_REGEX);

      if (!match) {
        return null;
      } // The regex match should return an array of length 2. The first value
      // is the 'PCXT=' and the second is the encoded context string value.


      return match[2];
    } catch (ex) {
      // eslint-disable-next-line no-console
      console.warn('Unable to read pricing context.');
      return null;
    }
  };

  var getContextString = function getContextString() {
    var cv = getRawCookieValue();

    if (!cv) {
      var defaultContext = getDefaultContext();
      setCookieContext(defaultContext);
      return encodeContextObject(defaultContext);
    }

    return cv;
  };
  /*
    Returns current context or the default for the current site.
  */


  var getCurrentContext = function getCurrentContext() {
    var cv = getRawCookieValue(); // If nothing is in the current cookie, set and return default.

    if (!cv) {
      var defaultContext = getDefaultContext();
      setCookieContext(defaultContext);
      return defaultContext;
    } // Return decoded context, otherwise set and return default.


    try {
      var context = unencodeContextString(cv);
      return context;
    } catch (ex) {
      // eslint-disable-next-line no-console
      console.warn('Unable to decode pricing context.');

      var _defaultContext = getDefaultContext();

      setCookieContext(_defaultContext);
      return _defaultContext;
    }
  };

  var setCoupon = function setCoupon(couponCode) {
    var context = getCurrentContext();
    context.couponCode = couponCode;
    setCookieContext(context);
  };

  var init = function init() {
    getCurrentContext();
  };
  /* --- API Methods --- */

  /*
  Gets the pricing context from the user's cookie. Returns the default
  context for the current site if no cookie is set.
  */


  contextModule.getPricingContext = function getPricingContext() {
    return getCurrentContext();
  };
  /*
  Gets the base 64 encoded pricing context string from the user's cookie. Returns the default
  context for the current site if no cookie is set.
  */


  contextModule.getEncodedContextString = function getEncodedContextString() {
    return getContextString();
  };
  /*
  Sets a coupon code on the user's pricing context cookie. Note: This will not apply the coupon to
  the shopper's cart.
   @param {String} couponCode - Coupon code the user has entered.
  */


  contextModule.setCouponCode = function setCouponCode(couponCode) {
    setCoupon(couponCode);
  };
  /*
  Gets the current coupon code on the user's pricing context cookie.
  */


  contextModule.getCouponCode = function getCouponCode() {
    var context = getCurrentContext();
    return context.couponCode;
  };
  /*
  Sets current vat inclusive state on the user's pricing context cookie.
   @param {Boolean} isInclusive - Whether the current state is VAT inclusive.
  */


  contextModule.setVatInclusive = function setVatInclusive(isInclusive) {
    var context = getCurrentContext();
    context.vatInclusive = isInclusive === true;
    setCookieContext(context);
  };
  /*
  Returns whether the user's pricing context is currently VAT inclusive.
  */


  contextModule.isVatInclusive = function isVatInclusive() {
    var context = getCurrentContext();
    return context.vatInclusive;
  };
  /* --- Module Definition --- */
  // Initialize the module with data from the current session's cookie


  init(); // If document is not available, just return the module.
  // During static site generation document will not be available.

  if (typeof document === 'undefined') {
    module.exports = contextModule;
    return;
  } // Add the module to the page


  document.pricingContextModule = contextModule;
})();