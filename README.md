# Autoapply Coupon POC

Flow example showing how to:
  * Land on page ``http://localhost:5000/ch/em/foo/bar?couponId=VP50``
  * Create a pricing context (cookie) for that coupon (VP50)
  * Redirect to page ``http://localhost:5000/foo/bar`` and show the applied coupon

## Run
* Optional steps (to use a virtual environment)
    * ``python -m venv my-env``
    * ``my-env\Scripts\activate.bat``
* ``pip install -r requirements.txt``
* ``env FLASK_APP=main.py flask run``