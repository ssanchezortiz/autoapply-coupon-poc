from flask import Flask, escape, request

app = Flask(__name__)


@app.route('/')
def root():
    return f'I am root!'


@app.route('/ch/em/<prod_category>/<prod_page>')
def prod_page_interceptor(prod_category, prod_page):
    html_content = ""
    with open('static_page.html', 'r') as reader:
        html_content = reader.read()
    return html_content


@app.route('/<prod_category>/<prod_page>')
def prod_page(prod_category, prod_page):
    html_content = ""
    with open('second_static_page.html', 'r') as reader:
        html_content = reader.read()
    return html_content


@app.route('/pricing-context-module.js')
def pricing_context_module():
    js_content = ""
    with open('pricing-context-module.js', 'r') as reader:
        js_content = reader.read()
    return js_content